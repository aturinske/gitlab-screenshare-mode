const NAME_SPACE = `screenshare-mode`;

const log = (one, ...rest) => console.debug(`${NAME_SPACE}:: ${one}`, ...rest);

let enabled = false;

chrome.storage.local.get(["enabled"], ({ enabled: storedEnabled = false }) => {
  log("Loaded state, enabled:", storedEnabled);
  enabled = storedEnabled;

  if (document.readyState === "complete") {
    log("DOM Content already loaded, re-initializing.");
    initialize();
  }
});

const injectStyle = () => {
  if (
    !enabled ||
    !document.documentElement ||
    document.getElementById("screenshare-mode-styles")
  ) {
    return;
  }
  log("injectStyle");
  const style = document.createElement("style");
  style.setAttribute("id", "screenshare-mode-styles");
  style.setAttribute("type", "text/css");
  // language=CSS
  style.textContent = `
      /* inject:start */
/* Confidential Issues Detail Page */
[data-page="projects:issues:show"]:not(.screenshare-mode-checked)
  .layout-page
  .content-wrapper
  > * {
  display: none !important;
}

[data-page="projects:issues:show"]:not(.screenshare-mode-checked)
  .layout-page
  .content-wrapper:before {
  content: "Checking if issue confidential...";
  position: absolute;
  padding-top: 2rem;
  padding-left: 1rem;
}

/* All other features */
.screenshare-mode-enabled .screenshare-mode-redacted > *,
#related-issues .list-item:not(.screenshare-mode-checked) .item-contents,
.issue-boards-content .board-card:not(.screenshare-mode-checked),
.issues-list .issue:not(.screenshare-mode-checked) .issue-box,
.timeline-entry.system-note:not(.screenshare-mode-checked) .timeline-content,
[data-qa-selector="projects_list"] .project-row:not(.screenshare-mode-checked) {
  display: none !important;
}

.screenshare-mode-enabled .screenshare-mode-redacted .screenshare-mode-show {
  display: block !important;
}

.screenshare-mode-enabled .screenshare-mode-redacted:before {
  content: "Redacted due to screenshare mode";
}

/* inject:end */`;
  (document.head || document.body || document.documentElement).appendChild(
    style
  );
};

chrome.runtime.onMessage.addListener((data) => {
  if (data.action === "injectStyle") {
    injectStyle();
  }
});

class Cleaner {
  constructor(name, selector, childSelector = null) {
    this.selector = selector;
    this.childSelector = childSelector;
    this.name = name;
  }

  static confidentialGitLabSpace = `gitlab-com/account-management`;

  cleanNode(node) {
    const isConfidential = Boolean(
      node.querySelector('[title="Confidential"]') ||
        node.querySelector('[data-original-title="Confidential"]') ||
        node.innerHTML.includes(Cleaner.confidentialGitLabSpace)
    );

    const target = this.childSelector
      ? node.querySelector(this.childSelector)
      : node;

    if (target) {
      target.classList.toggle(`${NAME_SPACE}-redacted`, isConfidential);
      node.classList.add(`${NAME_SPACE}-checked`);
    }
  }

  cleanAll() {
    const nodes = document.querySelectorAll(
      `${this.selector}:not(.${NAME_SPACE}-checked)`
    );

    log(
      `${this.constructor.name}:: Clean ${this.name}: ${nodes.length} nodes found`
    );

    nodes.forEach((node) => {
      this.cleanNode(node);
    });
  }
}

class IssueCheck {
  cleanAll() {
    if (document.body.classList.contains(`${NAME_SPACE}-checked`)) {
      return;
    }

    if (document.body.dataset.page === "projects:issues:show") {
      const warning = document.querySelector(".issuable-note-warning");
      const isConfidential = Boolean(
        warning &&
          warning.innerHTML.includes("/project/issues/confidential_issues.md")
      );
      log("confidential issue:", isConfidential, warning);

      if (isConfidential) {
        const issuableBody = document.querySelector(
          ".layout-page .content-wrapper"
        );

        issuableBody.classList.add(`${NAME_SPACE}-redacted`);

        const button = document.createElement("button");
        button.textContent = "Show issue content";
        button.classList.add(`${NAME_SPACE}-show`);
        button.onclick = () => {
          issuableBody.classList.remove(`${NAME_SPACE}-redacted`);
          button.remove();
        };
        issuableBody.appendChild(button);
      }
    }
    document.body.classList.add(`${NAME_SPACE}-checked`);
  }
}

const cleaners = [
  new IssueCheck(),
  new Cleaner(
    "System Notes",
    ".timeline-entry.system-note",
    ".timeline-content"
  ),
  new Cleaner("Related Issues", "#related-issues .list-item", ".item-contents"),
  new Cleaner(
    "Main Project List",
    '[data-qa-selector="projects_list"] .project-row'
  ),
  new Cleaner("Main Issue List", ".issues-list .issue", ".issue-box"),
  new Cleaner("Issue Boards", ".issue-boards-content .board-card"),
];

let observer = null;

function debounce(func, wait, immediate) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    }, wait);
    if (immediate && !timeout) func.apply(context, args);
  };
}

const cleanAll = debounce(() => {
  cleaners.forEach((cleaner) => cleaner.cleanAll());
}, 150);

const callback = (mutationList) => {
  mutationList.forEach((mutation) => {
    if (mutation && mutation.addedNodes && mutation.addedNodes.length) {
      cleanAll();
    }
  });
};

const enable = () => {
  log("Enabled called");
  injectStyle();
  cleanAll();
  if (observer) {
    observer.disconnect();
  }
  observer = new MutationObserver(callback);
  observer.observe(document.querySelector("body"), {
    childList: true,
    subtree: true,
  });
};

const disable = () => {
  document.getElementById("screenshare-mode-styles") &&
    document.getElementById("screenshare-mode-styles").remove();
  if (observer) {
    observer.disconnect();
  }
};

const initialize = () => {
  document.body.classList.toggle(`${NAME_SPACE}-enabled`, enabled);

  if (enabled) {
    enable();
  } else {
    disable();
  }

  renderButtonInNavigation();
};

const toggleState = () => {
  enabled = !enabled;
  chrome.storage.local.set({ enabled }, () => {
    log("Changed and saved state to:", enabled);
  });

  initialize();
};

const renderButtonInNavigation = () => {
  document.querySelectorAll(".screenshare-button").forEach((x) => x.remove());
  const button = document.createElement("button");
  button.textContent = `Screenshare mode: ${enabled ? "on" : "off"}`;
  button.onclick = toggleState;
  button.classList.add("screenshare-button");
  document
    .querySelector(".title-container .navbar-sub-nav")
    .appendChild(button);
};

window.addEventListener("DOMContentLoaded", () => {
  log("DOMContentLoaded");
  initialize();
});

if (window.module && window.module.exports) {
  module.exports = cleaners;
}
